// Copyright (c) OpenRobotGroup 2022.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.RobotBase;

/** Do not add any static variables to this class, or any initialization at all. */
public final class Main {
  private Main() {}

  /**
   * Main initialization function. Do not perform any initialization here.
   *
   * <p>Change the parameter type if the main robot class is changed.
   */
  public static void main(String... args) {
    RobotBase.startRobot(Robot::new);
  }
}
